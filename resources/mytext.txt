Having acquired the Power Stone from the planet Xandar, Thanos and his lieutenants—Ebony Maw, Cull Obsidian, Proxima
 Midnight, and Corvus Glaive—intercept a spaceship carrying the last survivors of Asgard. As they extract the Space
  Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Loki. Heimdall sends Hulk to Earth using
  the Bifröst before being killed. Thanos departs with his lieutenants and obliterates the ship.

Hulk crash-lands at the Sanctum Sanctorum in New York City, reverting to Bruce Banner. He warns Stephen Strange and
Wong about Thanos' plan to kill half of all life in the universe; in response, Strange recruits Tony Stark. Maw and
Obsidian arrive to retrieve the Time Stone from Strange, drawing the attention of Peter Parker. Maw captures Strange,
but fails to take the Time Stone due to an enchantment. Stark and Parker pursue Maw's spaceship, Banner contacts
 Steve Rogers, and Wong stays behind to guard the Sanctum.

In Edinburgh, Midnight and Glaive ambush Wanda Maximoff and Vision in order to retrieve the Mind Stone in Vision's
 forehead. Rogers, Natasha Romanoff, and Sam Wilson rescue them and take shelter with James Rhodes and Banner at
 the Avengers Compound. Vision offers to sacrifice himself by having Maximoff destroy the Mind Stone to keep Thanos
  from retrieving it. Rogers suggests they travel to Wakanda, which he believes has the resources to remove the stone
   without destroying Vision.

The Guardians of the Galaxy respond to a distress call from the Asgardian ship and rescue Thor, who surmises Thanos
 seeks the Reality Stone, which is in the possession of the Collector on Knowhere. Rocket and Groot accompany Thor to
 Nidavellir, where they and Eitri create a battle-axe capable of killing Thanos. On Knowhere, Peter Quill, Gamora,
 Drax, and Mantis find Thanos with the Reality Stone already in his possession. Thanos kidnaps Gamora, his adoptive
  daughter, who reveals the location of the Soul Stone to save her captive adoptive sister Nebula from torture.

olla, Groot, Maximoff, Wilson, Mantis, Drax, Quill, Strange, and Parker, as well as Maria Hill and Nick Fury,
 although Fury is able to transmit a signal first.[N 1] Stark and Nebula remain on Titan while Banner, M'Baku, Okoye,
  Rhodes, Rocket, Rogers, Romanoff, and Thor are left on the Wakandan battlefield. Meanwhile, Thanos watches a sunrise
   on another planet.