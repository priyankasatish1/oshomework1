import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public class WordThreader implements Callable<Map> {
    List<String> _lines;
    Integer numberOfWords = 0;
    public WordThreader(List<String> lines){
        _lines = lines;
    }
    public Map call(){
        Map<String,Integer> wordMap = new HashMap<>();
        for(String line: _lines)
        {
            if(line.isEmpty()){
                continue;
            }
            String[] words = line.trim().split(" ");
            for(String word : words)
            {
                word = word.toLowerCase().replaceAll("[^a-z0-9]","");
                if(wordMap.get(word)== null)
                {
                    wordMap.put(word,1);
                }
                else
                {
                    int keyValue = wordMap.get(word);
                    wordMap.put(word,keyValue+1);
                }
            }
            numberOfWords += words.length;
        }
        return wordMap;
    }
}
