import java.io.*;
import java.util.*;
import java.util.concurrent.*;

public class Word_Counter {
    private static int numOfSegments = 5;
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        ExecutorService executorPool = Executors.newFixedThreadPool(numOfSegments);
        List<Future<Map>> threadList = new ArrayList<>();
        SortedMap<String,Integer> compiledMap = new TreeMap<>();
        File myTextFile = new File("C:\\Users\\piyak\\IdeaProjects\\OS_Homework1\\src\\resources\\mytext.txt");
        FileInputStream fileInputStream = new FileInputStream(myTextFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream));
        String line;

        int totalLines = 0;
        while ((line = br.readLine()) != null) {
            totalLines++;
        }
        fileInputStream.getChannel().position(0);

        br = new BufferedReader(new InputStreamReader(fileInputStream));
        int linesPerSeg = totalLines/numOfSegments;
        int index = 0;
        List<String> chunk = new ArrayList<String >();
        while((line = br.readLine())!= null){
            index+=1;
            chunk.add(line);
            if(index == linesPerSeg){
                Callable<Map> callable = new WordThreader(chunk);
                Future<Map> future = executorPool.submit(callable);
                threadList.add(future);
                chunk = new ArrayList<>();
                index = 0;
            }
        }
        Callable<Map> callable = new WordThreader(chunk);
        Future<Map> futureLast = executorPool.submit(callable);
        threadList.add(futureLast);

        Map<String,Integer> wordCount ;
        for(Future<Map> future : threadList){
            wordCount = future.get();
            for(Map.Entry<String, Integer> entry : wordCount.entrySet()){
                Integer wCount = compiledMap.get(entry.getKey());
                if(wCount==null)
                {
                    compiledMap.put(entry.getKey(),entry.getValue());
                }
                else
                {
                    compiledMap.put(entry.getKey(),entry.getValue()+wCount);
                }
            }
        }
        executorPool.shutdownNow();
        Integer counter = 0;
        for(Map.Entry<String,Integer> tEntry : compiledMap.entrySet())
        {
            System.out.println(String.format("%-30s %s",tEntry.getKey(),tEntry.getValue()));
            counter += tEntry.getValue();
        }
        System.out.println("Total count = " + counter);
    }
}
